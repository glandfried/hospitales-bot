#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
token = str(np.loadtxt("token",dtype=str))
db = str(np.loadtxt("database",dtype=str))
clave = str(np.loadtxt("password",dtype=str))

import time
import logging
import requests
#import sys

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)#, InlineKeyboardMarkup) #InlineKeyboardButton
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler, ConversationHandler)

import  psycopg2

con = psycopg2.connect(database='ingresantes', user='hospital_bot', host='127.0.0.1', password=db)
c = con.cursor()
    


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

SELECCION, RECIBIR, ENVIAR, PASSWORD, VERIFICAR, IDENTIDAD, BIENVENIDE, HOSPITAL , VERIFICAR_HOSPITAL= range(9)
( NUMERO, VERIFICAR_NUMERO , NOMBRE, VERIFICAR_NOMBRE, TELEFONO, VERIFICAR_TELEFONO, EDAD
, VERIFICAR_EDAD, REGION, VERIFICAR_REGION, FIEBRE, VERIFICAR_FIEBRE, TOS, VERIFICAR_TOS
, GARGANTA, VERIFICAR_GARGANTA, SECRECIONES, VERIFICAR_SECRECIONES, RESPIRAR, VERIFICAR_RESPIRAR
, MUNICIPIO, VERIFICAR_MUNICIPIO, DOMICILIO, VERIFICAR_DOMICILIO, PRIVADOS, VERIFICAR_PRIVADOS
, VERIFICAR_DATOS,RECIBIR) = range(9,28+9)
END = -1 


def prueba(update, context):
    #canal_id = -1001431836706
    update.message.reply_text("Hola!, Esto es una prueba.")

def start(update, context):
    
    query = """
    select nombre, reportes
    from agente
    where agente_id={}
    """.format(update.message.from_user['id'])
    c.execute(query)#con.rollback()
    agente = c.fetchall()
    if len(agente)==0:
        update.message.reply_text("Te damos la bienvenida!")
        #update.message.reply_text("TODO: Verificar que le agente participe del canal de telegram para promotores de salud") 
        
        context.user_data['agente_id'] = update.message.from_user['id']
        context.user_data['nombre'] = update.message.from_user['first_name']
        context.user_data['apellido'] = update.message.from_user['last_name']
        
        update.message.reply_text("Envianos la clave para generar el usuario")
        
        return PASSWORD

    else:    
        update.message.reply_text('Hola {}!'.format(agente[0][0]))
        context.user_data['reportes']= agente[0][1] + 1
        if agente[0][1]==1:
            update.message.reply_text('Tenemos guardado {} reporte tuyo!'.format(agente[0][1]))
        else:
            update.message.reply_text('Tenemos guardado {} reportes tuyos!'.format(agente[0][1]))
        reply_keyboard = [['Recibir', 'Enviar', 'Simular']]
        
        
        update.message.reply_text('¿Qué querés hacer?', reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True) )
        update.message.reply_text('Recordá que siempre podés cancelar un reporte enviando la palabra "cancelar"')
        
        return SELECCION

def password(update,context):
    texto = update.message.text
    if texto.strip() == clave:
        update.message.reply_text("Envianos el nombre del Hospital")
        return HOSPITAL
    else:
        update.message.reply_text("La clave es incorrecta")
        #time.sleep(0.3)
        update.message.reply_text("Vuelva a intentarlo en 1 mintuo")
        time.sleep(60)
    return ConversationHandler.END
        
def hospital(update,context):
    texto = update.message.text
    update.message.reply_text('El Hospital se llama: *{}*'.format(texto))
    context.user_data['hospital'] = texto
    update.message.reply_text('¿Ese nombre es correcto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_HOSPITAL
    
def verificar_hospital(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text("Envianos el nombre del Hospital")
        return HOSPITAL
    if texto == 'Sí':
        nuevoAgente(update, context)
        bienvenide(update, context)
        return ConversationHandler.END

def nuevoAgente(update, context):
    	
    insert = """
    insert into agente 
    (agente_id, nombre, apellido, hospital, fecha, reportes )
    values (%s,%s,%s,%s,%s,%s)"""
    
    c.execute(insert,(
            context.user_data['agente_id'],
            context.user_data['nombre'],
            context.user_data['apellido'],
            context.user_data['hospital'],
            update.message.date,
            0)
    )
    
    con.commit()


def bienvenide(update, context):
    update.message.reply_text('Te damos la bienvenida!')
    #time.sleep(0.6)
    update.message.reply_text('Y te agradecemos por colaborar')
    #time.sleep(0.6)
    update.message.reply_text('La información que nos envies es muy importante para contener la pandemia')
    #time.sleep(0.6)
    update.message.reply_text('Escribime cada vez que llegue alguien nuevo al hospital')

def seleccion(update, context):
    if update.message.text == "Recibir":
        update.message.reply_text('¿Qué querés recibir?', reply_markup=ReplyKeyboardMarkup([['Formulario','Información']], one_time_keyboard=True))
        return RECIBIR
    if update.message.text == "Enviar":
        context.user_data['simulacro'] = False
        enviar(update, context)
        return IDENTIDAD    
    if update.message.text == "Simular":
        context.user_data['simulacro'] = True
        enviar(update, context)
        return IDENTIDAD
    

def chau(update, context):
    texto = 'Nos vemos! ESCRIBIR MENSAJE'
    
    user = update.message.from_user
    logger.info("El usuario %s canceló el reporte.", user.first_name)
    update.message.reply_text(texto,
                              reply_markup=ReplyKeyboardRemove())
    
    return ConversationHandler.END

def recibir(update,context):
    texto = update.message.text
    if texto == 'Formulario':
        formulario(update,context)
    if texto == 'Información':
        update.message.reply_text('Mirá este video')
        update.message.reply_text('https://www.youtube.com/watch?v=cV7fsxosrDc')
    chau(update,context)
    return END    

def formulario(update,context):
    update.message.reply_text('Te enviamos el formulario:')
    update.message.reply_text(
    """
Documento: Tipo y número 
Contacto: Teléfono o contacto
Edad: Años
Región Sanitaria: Del 1 al 12. 0 si no es de PBA
Fiebre: Si tiene o no
Tos seca: Si tiene o no
Dolor de garganta: Si tiene o no
Secreciones nasales: Si tiene o no
Dificultades para respirar: Si tiene o no
Tiene salud privada: Si tiene o no"""
    )
    
    
def tipo(update,context):
    reply_keyboard = [['DNI', 'Pasaporte', 'Otro']]
    
    update.message.reply_text(
        'Decinos el tipo de documento de la persona ingresante',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    )    

def enviar(update, context):

    context.user_data['agente_id'] = update.message.from_user['id']
    context.user_data['inicio'] = update.message.date
    #context.user_data['last_state'] = ConversationHandler.conversations['conversation_key']
    
    tipo(update,context)
    

def identidad(update, context):
    context.user_data['tipo'] = update.message.text
    update.message.reply_text('Envianos el número de documento')
    return NUMERO

def numero(update, context):
    texto = update.message.text
    if context.user_data['tipo']=='DNI':
        try:
            int(texto)
        except:
            update.message.reply_text('El DNI tiene que ser un número')
            tipo(update,context)
            return IDENTIDAD
    update.message.reply_text('El número de documento que enviaste es: *{}:{}*'.format(context.user_data['tipo'],texto))
    context.user_data['numero'] = texto
    #time.sleep(0.5)
    update.message.reply_text('¿Es correcto el tipo y el número de documento enviado?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    
    return VERIFICAR_NUMERO

def verificar_numero(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        tipo(update,context)
        return IDENTIDAD
    if texto == 'Sí':
        update.message.reply_text('Envianos el nombre completo del paciente')
        return NOMBRE

def nombre(update,context):
    texto = update.message.text
    
    update.message.reply_text('El nombre completo del paciente es: *{}*'.format(texto))
    context.user_data['nombre'] = texto
    #time.sleep(0.5)
    update.message.reply_text('¿Es correcto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_NOMBRE

def verificar_nombre(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('Envianos el nombre completo del paciente')
        return NOMBRE
    if texto == 'Sí':
        update.message.reply_text('Envianos el número de teléfono de un familiar o conocido del paciente')
        return TELEFONO

def telefono(update,context):
    texto = update.message.text
    
    update.message.reply_text('El número de teléfono (o medio de contacto) es: *{}*'.format(texto))
    context.user_data['contacto'] = texto
    #time.sleep(0.5)
    update.message.reply_text('¿Es correcto el contacto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_TELEFONO

def verificar_telefono(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('Envianos el número de teléfono de un familiar o conocido del paciente')
        return TELEFONO
    if texto == 'Sí':
        update.message.reply_text('Decinos la edad del paciente')
        return EDAD

def edad(update,context):
    texto = update.message.text
    try:
        update.message.reply_text('La edad del paciente es: *{}*'.format(int(texto)))
        context.user_data['edad'] = texto
        #time.sleep(0.5)
        update.message.reply_text('¿Es correcta la edad?',
            reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
        )
        return VERIFICAR_EDAD
    except:
        update.message.reply_text('Volvenos a decir la edad del paciente')
        update.message.reply_text('Ingresá solo números')
        return EDAD

def verificar_edad(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('Decinos la edad del paciente')
        return EDAD
    if texto == 'Sí':
        update.message.reply_text('Decinos de que región sanitaria es el paciente (regiones del 1 al 12)')
        update.message.reply_text('Marcá 0 si no es de la Provincia de Buenos Aires')
        return REGION

def region(update,context):
    texto = update.message.text
    try:
        res = int(texto)
        if res >= 0 and  res <= 12:
            update.message.reply_text('El paciente es de la *región sanitaria número {}*'.format(res))
            context.user_data['region'] = texto
            #time.sleep(0.5)
            update.message.reply_text('¿Es correcto?',
                reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
            )
            return VERIFICAR_REGION
        else:
            update.message.reply_text('El número estuvo fuera de rango')
            update.message.reply_text('Decinos de que región sanitaria es el paciente (regiones del 1 al 12)')
            update.message.reply_text('Marcá 0 si no es de la Provincia de Buenos Aires')
            return REGION
    except:
        update.message.reply_text('Decinos de que región sanitaria es el paciente')
        update.message.reply_text('Tiene que ser un número del 1 al 12, o 0 si no es de la Provincia de Buenos Aires')
        return REGION
    
def verificar_region(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('Decinos de que región sanitaria es el paciente (regiones del 1 al 12)')
        update.message.reply_text('Marcá 0 si no es de la Provincia de Buenos Aires')
        return REGION
    if texto == 'Sí':
        update.message.reply_text('¿El paciente tiene fiebre?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return FIEBRE

def fiebre(update,context):
    texto = update.message.text
    update.message.reply_text('El paciente *{} tiene fiebre*'.format(texto))
    context.user_data['fiebre'] = True if texto == 'Sí' else False
    #time.sleep(0.3)
    update.message.reply_text('¿Es correcto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_FIEBRE

def verificar_fiebre(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('¿El paciente tiene fiebre?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return FIEBRE
    if texto == 'Sí':
        update.message.reply_text('¿El paciente tiene tos seca?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return TOS

def tos(update,context):
    texto = update.message.text
    update.message.reply_text('El paciente *{} tiene tos seca*'.format(texto))
    context.user_data['tos'] = True if texto == 'Sí' else False
    #time.sleep(0.3)
    update.message.reply_text('¿Es correcto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_TOS

def verificar_tos(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('¿El paciente tiene tos seca?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return TOS
    if texto == 'Sí':
        update.message.reply_text('¿El paciente tiene dolor de garganta?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return GARGANTA
    
def garganta(update,context):
    texto = update.message.text
    update.message.reply_text('El paciente *{} tiene dolor de garganta*'.format(texto))
    context.user_data['garganta'] = True if texto == 'Sí' else False
    #time.sleep(0.3)
    update.message.reply_text('¿Es correcto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_GARGANTA

def verificar_garganta(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('¿El paciente tiene dolor de garganta?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return GARGANTA
    if texto == 'Sí':
        update.message.reply_text('¿El paciente tiene secreciones nasales?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return SECRECIONES

def secreciones(update,context):
    texto = update.message.text
    update.message.reply_text('El paciente *{} tiene secreciones nasales*'.format(texto))
    context.user_data['secreciones'] = True if texto == 'Sí' else False
    #time.sleep(0.3)
    update.message.reply_text('¿Es correcto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_SECRECIONES

def verificar_secreciones(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('¿El paciente tiene secreciones nasales?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return SECRECIONES
    if texto == 'Sí':
        update.message.reply_text('¿El paciente tiene dificultades para respirar?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return RESPIRAR
    
def respirar(update,context):
    texto = update.message.text
    update.message.reply_text('El paciente *{} tiene dificultades para respirar*'.format(texto))
    context.user_data['respirar'] = True if texto == 'Sí' else False
    #time.sleep(0.3)
    update.message.reply_text('¿Es correcto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_RESPIRAR

def verificar_respirar(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('¿El paciente tiene dificultades para respirar?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return RESPIRAR
    if texto == 'Sí':
        update.message.reply_text('Decinos en qué municipio vive el paciente')
        return MUNICIPIO

def municipio(update,context):
    texto = update.message.text
    update.message.reply_text('El paciente es del *municipio: {}*'.format(texto))
    context.user_data['municipio'] = texto
    #time.sleep(0.3)
    update.message.reply_text('¿Es correcto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_MUNICIPIO

def verificar_municipio(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('Decinos en qué municipio vive el paciente')
        return MUNICIPIO
    if texto == 'Sí':
        update.message.reply_text('Danos el domicilio del paciente, la calle, la altura y vivienda')
        return DOMICILIO

def domicilio(update,context):
    texto = update.message.text
    update.message.reply_text('El domicilio del paciente *{}*'.format(texto))
    context.user_data['domicilio'] = texto
    #time.sleep(0.3)
    update.message.reply_text('¿Es correcto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_DOMICILIO

def verificar_domicilio(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('Danos el domicilio del paciente, la calle, la altura y vivienda')
        return DOMICILIO
    if texto == 'Sí':
        update.message.reply_text('¿El paciente tiene obra social o prepaga?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return PRIVADOS

def privados(update,context):
    texto = update.message.text
    update.message.reply_text('El paciente *{} tiene obra social o prepaga*'.format(texto))
    context.user_data['privados'] = True if texto == 'Sí' else False
    #time.sleep(0.3)
    update.message.reply_text('¿Es correcto?',
        reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
    )
    return VERIFICAR_PRIVADOS

def imprimir_datos(update,context):
    update.message.reply_text("Los datos cargados son:")
    update.message.reply_text("""
Documento: {} {}
Contacto: {}
Edad: {}
Región Sanitaria: {}
Fiebre: {}
Tos seca: {}
Dolor de garganta: {}
Secreciones nasales: {}
Dificultades para respirar: {}
Municipio: {}
Domicilio: {}
Tiene salud privada: {}""".format(context.user_data['tipo'],
            context.user_data['numero'],
            context.user_data['contacto'],
            context.user_data['edad'],
            context.user_data['region'],
            context.user_data['fiebre'],
            context.user_data['tos'],
            context.user_data['garganta'],
            context.user_data['secreciones'],
            context.user_data['respirar'],
            context.user_data['municipio'],
            context.user_data['domicilio'],
            context.user_data['privados'])
    )
def verificar_privados(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No':
        update.message.reply_text('¿El paciente tiene obra social o prepaga?',reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True) )
        return PRIVADOS
    if texto == 'Sí':
        update.message.reply_text('Verificá los datos')
        imprimir_datos(update,context)
        update.message.reply_text('¿Querés guardar estos datos?',
            reply_markup=ReplyKeyboardMarkup([['No', 'Sí']], one_time_keyboard=True)
        )
        return VERIFICAR_DATOS

def verificar_datos(update,context):
    texto = update.message.text
    #time.sleep(0.3)
    if texto == 'No' or context.user_data['simulacro']:
        chau(update,context)
        return END
    if texto == 'Sí':
        guardar(update,context)
        chau(update,context)
        return END
    
def guardar(update,context):
    insert = """
    insert into reporte 
    (agente_id, tipo, documento, contacto, edad, region, fiebre, tos_seca, dolor_garganta, secreciones_nasales, dificultad_respirar, municipalidad, domicilio, salud_privada, inicio, final)
    values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    res = ( context.user_data['agente_id'],
            context.user_data['tipo'],
            context.user_data['numero'],
            context.user_data['contacto'],
            context.user_data['edad'],
            context.user_data['region'],
            context.user_data['fiebre'],
            context.user_data['tos'],
            context.user_data['garganta'],
            context.user_data['secreciones'],
            context.user_data['respirar'],
            context.user_data['municipio'],
            context.user_data['domicilio'],
            context.user_data['privados'],
            context.user_data['inicio'],
            update.message.date
    )
    c.execute(insert,res)
    
    update = """
    UPDATE agente
    SET reportes = {}
    WHERE agente_id = {}
    """.format(context.user_data['reportes'],context.user_data['agente_id'])
    c.execute(update)
    
    con.commit()
    return res
    
    
# Create the Updater and pass it your bot's token.
# Make sure to set use_context=True to use the new context based callbacks
# Post version 12 this will no longer be necessary
updater = Updater(token, use_context=True)

# Get the dispatcher to register handlers
dp = updater.dispatcher

# Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
conv_handler = ConversationHandler(
    entry_points=[MessageHandler(Filters.text, start)],

    states={
        PASSWORD:[MessageHandler(Filters.text, password)],
        HOSPITAL:[MessageHandler(Filters.text, hospital)],
        VERIFICAR_HOSPITAL:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_hospital)],
        SELECCION:[MessageHandler(Filters.regex('^(Recibir|Enviar|Simular)$'), seleccion)],
        RECIBIR:[MessageHandler(Filters.regex('^(Formulario|Información)$'), recibir)],
        IDENTIDAD:[MessageHandler(Filters.regex('^(DNI|Pasaporte|Otro)$'), identidad)],
        NUMERO:[MessageHandler(Filters.text, numero)],
        VERIFICAR_NUMERO:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_numero)],
        NOMBRE:[MessageHandler(Filters.text, nombre)],
        VERIFICAR_NOMBRE:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_nombre)],
        TELEFONO:[MessageHandler(Filters.text, telefono)],
        VERIFICAR_TELEFONO:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_telefono)],
        EDAD:[MessageHandler(Filters.text, edad)],
        VERIFICAR_EDAD:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_edad)],
        REGION:[MessageHandler(Filters.text, region)],
        VERIFICAR_REGION:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_region)],
        FIEBRE:[MessageHandler(Filters.text, fiebre)],
        VERIFICAR_FIEBRE:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_fiebre)],
        TOS:[MessageHandler(Filters.text, tos)],
        VERIFICAR_TOS:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_tos)],
        GARGANTA:[MessageHandler(Filters.text, garganta)],
        VERIFICAR_GARGANTA:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_garganta)],
        SECRECIONES:[MessageHandler(Filters.text, secreciones)],
        VERIFICAR_SECRECIONES:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_secreciones)],
        RESPIRAR:[MessageHandler(Filters.text, respirar)],
        VERIFICAR_RESPIRAR:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_respirar)],
        MUNICIPIO:[MessageHandler(Filters.text, municipio)],
        VERIFICAR_MUNICIPIO:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_municipio)],
        DOMICILIO:[MessageHandler(Filters.text, domicilio)],
        VERIFICAR_DOMICILIO:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_domicilio)],
        PRIVADOS:[MessageHandler(Filters.text, privados)],
        VERIFICAR_PRIVADOS:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_privados)],
        VERIFICAR_DATOS:[MessageHandler(Filters.regex('^(Sí|No)$'), verificar_datos)],
    },

    fallbacks=[MessageHandler(Filters.regex('^(cancelar|Cancelar|salir|stop|quit|exit)$'), chau), CallbackQueryHandler(chau, pattern='^' + str(END) + '$')]
)


dp.add_handler(conv_handler)

# log all errors
#dp.add_error_handler(error)

# Start the Bot
updater.start_polling()

# Run the bot until you press Ctrl-C or the process receives SIGINT,
# SIGTERM or SIGABRT. This should be used most of the time, since
# start_polling() is non-blocking and will stop the bot gracefully.
updater.idle()



