create table agente (
	agente_id int primary key,
	nombre varchar(128) not null,
	apellido varchar(128) not null,
	hospital varchar(128) not null,
	fecha TIMESTAMP not null,
	reportes int not null
);

