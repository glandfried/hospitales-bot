--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: identidad; Type: TYPE; Schema: public; Owner: hospital_bot
--

CREATE TYPE identidad AS ENUM (
    'DNI',
    'Pasaporte',
    'Otro'
);


ALTER TYPE public.identidad OWNER TO hospital_bot;

--
-- Name: region; Type: TYPE; Schema: public; Owner: hospital_bot
--

CREATE TYPE region AS ENUM (
    'I',
    'II',
    'III',
    'IV',
    'V',
    'VI',
    'VII',
    'VIII',
    'IX',
    'X',
    'XI',
    'XII',
    'Otra'
);


ALTER TYPE public.region OWNER TO hospital_bot;

--
-- Name: tipo_identidad; Type: TYPE; Schema: public; Owner: hospital_bot
--

CREATE TYPE tipo_identidad AS ENUM (
    'DNI',
    'Pasaporte',
    'Otra'
);


ALTER TYPE public.tipo_identidad OWNER TO hospital_bot;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agente; Type: TABLE; Schema: public; Owner: hospital_bot; Tablespace: 
--

CREATE TABLE agente (
    agente_id integer NOT NULL,
    nombre character varying(64) NOT NULL,
    apellido character varying(64) NOT NULL,
    hospital character varying(64) NOT NULL,
    fecha timestamp without time zone NOT NULL,
    reportes integer NOT NULL
);


ALTER TABLE public.agente OWNER TO hospital_bot;

--
-- Name: reporte; Type: TABLE; Schema: public; Owner: hospital_bot; Tablespace: 
--

CREATE TABLE reporte (
    clave integer NOT NULL,
    agente_id integer NOT NULL,
    tipo identidad NOT NULL,
    documento character varying(34) NOT NULL,
    nombre character varying(128),
    contacto character varying(128) NOT NULL,
    edad integer NOT NULL,
    region integer NOT NULL,
    fiebre boolean NOT NULL,
    tos_seca boolean NOT NULL,
    dolor_garganta boolean NOT NULL,
    secreciones_nasales boolean NOT NULL,
    dificultad_respirar boolean NOT NULL,
    municipalidad character varying(64),
    domicilio character varying(128),
    salud_privada boolean,
    inicio timestamp without time zone NOT NULL,
    final timestamp without time zone NOT NULL
);


ALTER TABLE public.reporte OWNER TO hospital_bot;

--
-- Name: reporte_clave_seq; Type: SEQUENCE; Schema: public; Owner: hospital_bot
--

CREATE SEQUENCE reporte_clave_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reporte_clave_seq OWNER TO hospital_bot;

--
-- Name: reporte_clave_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hospital_bot
--

ALTER SEQUENCE reporte_clave_seq OWNED BY reporte.clave;


--
-- Name: clave; Type: DEFAULT; Schema: public; Owner: hospital_bot
--

ALTER TABLE ONLY reporte ALTER COLUMN clave SET DEFAULT nextval('reporte_clave_seq'::regclass);


--
-- Data for Name: agente; Type: TABLE DATA; Schema: public; Owner: hospital_bot
--

COPY agente (agente_id, nombre, apellido, hospital, fecha, reportes) FROM stdin;
657804218	Gustavo	Landfried	programador	2020-03-26 22:42:16	0
\.


--
-- Data for Name: reporte; Type: TABLE DATA; Schema: public; Owner: hospital_bot
--

COPY reporte (clave, agente_id, tipo, documento, nombre, contacto, edad, region, fiebre, tos_seca, dolor_garganta, secreciones_nasales, dificultad_respirar, municipalidad, domicilio, salud_privada, inicio, final) FROM stdin;
\.


--
-- Name: reporte_clave_seq; Type: SEQUENCE SET; Schema: public; Owner: hospital_bot
--

SELECT pg_catalog.setval('reporte_clave_seq', 1, false);


--
-- Name: agente_pkey; Type: CONSTRAINT; Schema: public; Owner: hospital_bot; Tablespace: 
--

ALTER TABLE ONLY agente
    ADD CONSTRAINT agente_pkey PRIMARY KEY (agente_id);


--
-- Name: reporte_pkey; Type: CONSTRAINT; Schema: public; Owner: hospital_bot; Tablespace: 
--

ALTER TABLE ONLY reporte
    ADD CONSTRAINT reporte_pkey PRIMARY KEY (clave);


--
-- Name: reporte_agente_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hospital_bot
--

ALTER TABLE ONLY reporte
    ADD CONSTRAINT reporte_agente_id_fkey FOREIGN KEY (agente_id) REFERENCES agente(agente_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

