create type identidad as enum ('DNI', 'Pasaporte', 'Otro');

create table reporte (
	clave serial primary key,
	agente_id int not null,
	tipo  identidad not null,
	documento varchar(34) not null,
	nombre varchar(128),
	contacto varchar(128) not null,
	edad int not null,
	region int not null,
	fiebre bool not null,
	tos_seca bool not null,
	dolor_garganta bool not null,
	secreciones_nasales bool not null,
	dificultad_respirar bool not null,
	municipalidad varchar(64),
	domicilio varchar(128),
	salud_privada bool,
	inicio TIMESTAMP not null,
	final TIMESTAMP not null,
	foreign key (agente_id)  references agente (agente_id)
);

